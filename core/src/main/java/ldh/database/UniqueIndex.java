package ldh.database;

import java.util.List;

public class UniqueIndex {

	private List<String> columnNames;
	
	private List<Column> columns;
	
	private String indexName;
	
	public UniqueIndex(String indexName, List<String> columnNames) {
		this.indexName = indexName;
		this.columnNames = columnNames;
	}

	public List<String> getColumnNames() {
		return columnNames;
	}

	public void setColumnNames(List<String> columnNames) {
		this.columnNames = columnNames;
	}

	public String getIndexName() {
		return indexName;
	}

	public void setIndexName(String indexName) {
		this.indexName = indexName;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}
	
	public boolean isPrimaryKey() {
		return indexName.equalsIgnoreCase("primary");
	}
}
