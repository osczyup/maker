package ldh.util;

public class Constant {

	public static final int HalfAnHour_Mill= 1000 * 60 * 5;
	public static final long Day_Mill = 1000 * 60 * 60 * 24L;
	public static final int Hour_Mill = 1000 * 60 * 60;
	public static final int Min_Mill = 1000 * 60;
	
	public static final String yyyyMMddHH = "yyyyMMddHH";
	
	public static final String yyyyMMddHHmm = "yyyyMMddHHmm";
	
	public static final String yyyyMMddHHmmss = "yyyyMMddHHmmss";
	
	public static final String yyyyMMdd = "yyyyMMdd";
	
	public static final String yyyyMM = "yyyyMM";
	
	public static final String yyyy_MM_dd_HH_mm_ss = "yyyy-MM-dd HH:mm:ss";
	
	public static final String yyyy_MM_dd = "yyyy-MM-dd";
}