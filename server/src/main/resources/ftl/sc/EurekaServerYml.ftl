spring:
  application:
    name: eureka-server
  profiles: server1
  security:
    basic:
      enabled: true
    user:
      name: admin
      password: admin

server:
  port: ${port?c}

eureka:
  instance:
    hostname: ${host}
  client:
    registerWithEureka: false
    fetchRegistry: false
    <#if !isOne>
    service-url:
       defaultZone: ${serviceUrl}
    </#if>
