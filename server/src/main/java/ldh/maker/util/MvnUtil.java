package ldh.maker.util;

import org.apache.maven.shared.invoker.*;

import java.io.File;
import java.util.Collections;
import java.util.function.BiConsumer;

public class MvnUtil {

    public static void runMvn(String mvn, String pom, BiConsumer<String, Throwable> logger) {
        InvocationRequest request = new DefaultInvocationRequest();
        request.setPomFile(new File(pom));
        request.setGoals(Collections.singletonList(mvn));

        Invoker invoker = new DefaultInvoker();
        String mavnHome = System.getenv("MAVEN_HOME");
        System.out.println("sfa:" + mavnHome);
        invoker.setMavenHome(new File(mavnHome));
        try {
            InvocationResult invocationResult = invoker.execute(request);
            invoker.setLogger(new InvokerLogger() {
                @Override
                public void debug(String s) {
                    logger.accept(s, null);
                }

                @Override
                public void debug(String s, Throwable throwable) {
                    logger.accept(s, throwable);
                }

                @Override
                public boolean isDebugEnabled() {
                    return false;
                }

                @Override
                public void info(String s) {
                    logger.accept(s, null);
                }

                @Override
                public void info(String s, Throwable throwable) {
                    logger.accept(s, throwable);
                }

                @Override
                public boolean isInfoEnabled() {
                    return false;
                }

                @Override
                public void warn(String s) {
                    logger.accept(s, null);
                }

                @Override
                public void warn(String s, Throwable throwable) {
                    logger.accept(s, throwable);
                }

                @Override
                public boolean isWarnEnabled() {
                    return false;
                }

                @Override
                public void error(String s) {
                    logger.accept(s, null);
                }

                @Override
                public void error(String s, Throwable throwable) {
                    logger.accept(s, throwable);
                }

                @Override
                public boolean isErrorEnabled() {
                    return false;
                }

                @Override
                public void fatalError(String s) {
                    logger.accept(s, null);
                }

                @Override
                public void fatalError(String s, Throwable throwable) {
                    logger.accept(s, throwable);
                }

                @Override
                public boolean isFatalErrorEnabled() {
                    return false;
                }

                @Override
                public void setThreshold(int i) {

                }

                @Override
                public int getThreshold() {
                    return 0;
                }
            });
        } catch (MavenInvocationException e) {
            e.printStackTrace();
        }
    }
}
