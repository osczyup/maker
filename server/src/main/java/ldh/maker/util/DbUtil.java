package ldh.maker.util;

import java.math.BigDecimal;
import java.util.*;

/**
 * Created by ldh on 2017/3/26.
 */
public class DbUtil {

    private static Map<String, Class<?>> javaTypeMap = new HashMap<>();
    static{
        javaTypeMap.put("Integer", Integer.class);
        javaTypeMap.put("Long", Long.class);
        javaTypeMap.put("Short", Short.class);
        javaTypeMap.put("Byte", Byte.class);
        javaTypeMap.put("Byte[]", Byte[].class);
        javaTypeMap.put("Double", Double.class);
        javaTypeMap.put("BigDecimal", BigDecimal.class);
        javaTypeMap.put("Float", Float.class);
        javaTypeMap.put("Date", Date.class);
        javaTypeMap.put("String", String.class);
        javaTypeMap.put("Boolean", Boolean.class);
        javaTypeMap.put("Enum", Enum.class);
    }

    public static Set<String> javaType() {
        return javaTypeMap.keySet();
    }

    public static Class<?> javaClass(String javaType) {
        return javaTypeMap.get(javaType);
    }
}
