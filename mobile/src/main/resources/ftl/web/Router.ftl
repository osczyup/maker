import ListPage from '@/pages/List'
import ViewPage from '@/pages/View'

const routes = [
{ path: '${util.firstLower(table.javaName)}/list', component:ListPage },
{ path: '${util.firstLower(table.javaName)}/view', component:ViewPage }
]