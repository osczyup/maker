package ldh.maker.component;

import javafx.scene.control.Tab;
import javafx.scene.control.TreeItem;
import ldh.maker.vo.TreeNode;

/**
 * Created by ldh123 on 2018/5/6.
 */
public class BootstrapSettingPane extends SettingPane {

    private BootstrapTableUi bootstrapTableUi;

    public BootstrapSettingPane(TreeItem<TreeNode> treeItem, String dbName) {
        super(treeItem, dbName);

        buildTableUiTab();
    }

    private void buildTableUiTab() {
        bootstrapTableUi = new BootstrapTableUi(treeItem, dbName);
        Tab tab = createTab("Table设置", bootstrapTableUi);
        tab.selectedProperty().addListener((b,o,n)->{
            if (tab.isSelected()) {
                bootstrapTableUi.show();
            }
        });
    }
}
