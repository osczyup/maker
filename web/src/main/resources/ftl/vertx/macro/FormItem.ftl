<#macro FormItem name label>

<div class="form-group">
    <label for="${name}" class="col-sm-2 control-label">${label}</label>
    <div class="col-sm-10">
        <#nested>
    </div>
</div>
</#macro>