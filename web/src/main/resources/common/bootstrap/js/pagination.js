﻿var ctx = "/";

function $paginationSelect(formId){
	$("#" + formId).submit();
}

function $pagination(formId, page){
	$("#" + formId + "_pageNo").val(page);
	$("#" + formId).submit();
}

function $paginationFirst(formId){
	$("#" + formId + "_pageNo").val("1");
	$("#" + formId).submit();
}

function $paginationPre(formId){
	var length = $("#" + formId + "_pageNo").val();
	length = length - 1;
	if (length < 1) length = 1;
	$("#" + formId + "_pageNo").val(length);
	$("#" + formId).submit();
}

function $paginationNext(formId, pageTotal){
	var length = $("#" + formId + "_pageNo").val();
	length = length + 1;
	if (length > pageTotal) length = pageTotal;
	$("#" + formId + "[name='pageNo']")[0] = length;
	$("#" + formId).submit();
}

